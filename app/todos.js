const todos = [
  { title: `TODOs` },
  { title: `ACCs` },
  { title: 'Check the logic line by line' },
  { title: 'possible NPEs' },
  { title: 'Tests (Unit/IT)' },
  { title: 'Test in all browsers' }, // TODO-DMA sublist all browsers?
  { title: 'Test various viewport sizes and devices (iOS, Android)' },
]

export const getTodos = () => todos.map(t => Object.assign({}, t))
import { getTodos } from './todos.js';

let storedTodos;

try {
  storedTodos = JSON.parse(localStorage.getItem('todos'))
} catch (e) {
  console.error(e)
  localStorage.removeItem('todos')
}

const app = window.app = new Vue({
  el: '#app',
  data: {
    title: 'What to check before to-review ticket transition:',
    todos: storedTodos || getTodos()
  },
  computed: {
    canComplete() {
      return this.todos.every(t => t.checked)
    }
  },
  methods: {
    onSelection() {
      if (Array.isArray(app.todos)) {
        localStorage.setItem('todos', JSON.stringify(app.todos))
      }
    },
    resetTodos() {
      localStorage.removeItem('todos')
      this.todos = getTodos()
    }
  }
})

window.addEventListener('beforeunload', e => {
  if (Array.isArray(app.todos)) {
    localStorage.setItem('todos', JSON.stringify(app.todos))
  }
})
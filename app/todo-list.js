Vue.component('todo-list', {
  props: ['todos'],
  template: `
    <ul class="todo-list">
      <todo-item v-for="todo of todos" 
                 v-bind:todo="todo"
                 v-bind:key="todo.title"
                 v-on:selection="$emit('selection')">
      </todo-item>
    </ul>
  `
})
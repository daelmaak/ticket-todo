Vue.component('todo-item', {
  props: ['todo'],
  template: `
    <li class="todo-item">
      <label>
        <input type="checkbox" 
               class="filled-in checkbox-pink"
               v-model="todo.checked"
               v-on:change="$emit('selection')">
        <span>{{ todo.title }}</span>
      </label>
    </li>
  `
})